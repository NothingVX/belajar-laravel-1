<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    function awal(){
        return view('welcome');
    }
    function home(){
        return view('home');
    }
    function form(){
        return view('form');
    }
}
