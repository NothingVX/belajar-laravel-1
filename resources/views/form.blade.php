<html>

<head>
    <title>Buat Akun</title>
</head>

<body>
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/kirim" method="POST">
        @csrf
        <label>First name:</label><br>
        <input type="text" name="fname" id="fname"><br><br>
        <label>Last name:</label><br>
        <input type="text" name="lname" id="lname"><br>

        <br><label>Gender:</label> <br>
        <input type="radio" id="male" name="fav_language" value="male"><label>Male</label> <br>
        <input type="radio" id="female" name="fav_language" value="female"><label>Female</label> <br>
        <input type="radio" id="other" name="fav_language" value="other"><label>Other</label> <br> <br>

        <label>Nationality:</label>
        <select name="kewarganegaraan" id="">
            <option value="indo">Indonesia</option>
            <option value="eng">Inggris</option>
            <option value="frnc">Perancis</option>
            <option value="swss">Swiss</option>
        </select> <br> <br>

        <label>Language Spoken:</label><br>
            <input type="checkbox" id="bahasa" name="ckbox" value="bahasa"><label>Bahasa Indonesia</label> <br>
            <input type="checkbox" id="english" name="ckbox" value="english"><label>English</label> <br>
            <input type="checkbox" id="arabic" name="ckbox" value="arabic"><label>Arabic</label><br>
            <input type="checkbox" id="other" name="ckbox" value="other"><label>Other</label><br><br>

        <label>Bio:</label><br>
        <textarea name="txtarea" id="bio" cols="30" rows="10" method="POST"></textarea><br> <br>
        <input type="submit" value="Sign Up">
    </form>
</body>

</html>